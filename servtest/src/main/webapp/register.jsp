<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hello World</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">    <link rel='stylesheet' type='text/css' href='//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css'>
</head>
<body style="background-color: #e1e8e1">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="add.html"><b>Sign up!</a><br/><br/>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
    </div>
</nav>
<form action="register" method="get">
  <div class="form-row">
     <div class="form-group col-md-4">
    <label for="inputUsername">Username</label>
    <input type="text" class="form-control" id="username" name="username">
  </div>
     <div class="form-group col-md-4">
    <label for="inputPassword">Password</label>
    <input type="text" class="form-control" id="password" name="password">
  </div>
  <div class="form-group col-md-3">
    <label for="inputAddress">Confirm password</label>
    <input type="text" class="form-control" id="password" name="password">
  </div>
  <div class="form-group col-md-4">
    <label for="inputEmail">Email address</label>
    <input type="text" class="form-control" id="email" name="email">
  </div>
</div>
  <input type="submit" class="btn btn-primary"/>
  
</form>
</body>
</html>