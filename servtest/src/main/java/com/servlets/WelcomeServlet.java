package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class WelcomeServlet
 */
@WebServlet("/WelcomeServlet")
public class WelcomeServlet extends HttpServlet {
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)  
		    throws ServletException, IOException {  
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String title = "Schedulde account";
	      String docType =
	         "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";

	      out.println(docType +
	         "<html>\n" +
	         "<head><title>" + title + "</title></head>\n" +
	         "<body bgcolor = \"#f0f0f0\">\n" +
	         "<h1 align = \"center\">" + title + "</h1>\n" +
	         "<table width = \"100%\" border = \"1\" align = \"center\">\n" +
	         "<tr bgcolor = \"#949494\">\n" +
	         "    <th>Nr raty</th>" + 
				"    <th>Kwota Kapitalu</th> " + 
				"    <th>Kwota odsetek</th>" + 
				"    <th>Oplaty stale</th>" + 
				"    <th>Calkowita kwota raty</th>" + 
				"  </tr>" 
				+"</table>"
	      );
	}
		
		
	      public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
	  		
		
		    response.setContentType("text/html");  
	    PrintWriter out = response.getWriter();  
		          
		    String n=request.getParameter("username");  
		    out.print("Welcome "+n);  
		          
		    out.close();  
		    }  
}
