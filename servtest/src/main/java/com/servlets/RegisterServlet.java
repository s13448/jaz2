package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
  
response.setContentType("text/html");  
PrintWriter out = response.getWriter();  
          
String n=request.getParameter("username");  
String p=request.getParameter("password");  
String e=request.getParameter("email");  
  
          
try{  
Class.forName("org.hsqldb.jdbc.JDBCDriver");  
Connection con=DriverManager.getConnection(  
"jdbc:hsqldb:hsql://localhost/workdb","SA","");  
  
PreparedStatement ps=con.prepareStatement(  
"insert into users values(?,?,?,?)");  
  
ps.setString(1,n);  
ps.setString(2,p);  
ps.setString(3,e);  
          
int i=ps.executeUpdate();  
if(i>0)  
out.print("You are successfully registered...");  
      
          
}catch (Exception e2) {System.out.println(e2);}  
          
out.close();  
}  
	
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
	
	    response.setContentType("text/html");  
  PrintWriter out = response.getWriter();  
	          
	    String n=request.getParameter("username");  
	    out.print("Welcome "+n);  
	          
	    out.close();  
	    }  

  
}  