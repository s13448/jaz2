package model;

public class Rata {
	
	public int nrRaty;
	public float rata;
	public float kwotaMalejaca;
	public float odsetki;
	public String oplataStala;
	
	public Rata(int nrRaty, float rata, float kwotaMalejaca, float odsetki, String oplataStala) {
		this.nrRaty = nrRaty;
		this.rata = rata;
		this.kwotaMalejaca = kwotaMalejaca;
		this.odsetki = odsetki;
		this.oplataStala = oplataStala;
	}

}
